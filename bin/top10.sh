#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "Usage: top10.sh <path to data file>"
fi


pig -param input=$1 -param top_n=10 pig_scripts/top_n_by_lang.pig 2>/dev/null
