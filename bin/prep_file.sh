#!/bin/bash

if [[ $# -ne 2 ]]; then
  echo "Usage: prep_file.sh <path to input file> <path to write output file>"
fi

pig -param input=$1 -param output=$2 pig_scripts/prep.pig 2>/dev/null

echo "OK, data is ready at $1"