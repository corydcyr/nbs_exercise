#!/bin/bash

if [[ $# -ne 2 ]]; then
  echo "Usage: top10_for_lang.sh <path to data file> <two letter language code>"
fi


pig -param input=$1 -param top_n=10 -param lang=$2 pig_scripts/top_n_for_lang.pig 2>/dev/null
