##Code Challenge For Next Big Sound##


#requirements#

To keep it simple I used Pig for pre-processing and querying the data,
so a working install of Pig is required to run this.

The wrapper shell scripts assume that Pig is available in your $PATH env variable.



#setup#

 - after downloading or checking out this project
 - cd into the top level folder
 - `chmod +x bin/*`

#prep the data#

 - if Pig is running in MR mode the input file needs to be copied to hdfs first.
 - `bin/prep_file.sh <path to data file> <path to write prepped data to>`

#query the data#
 - `bin/top10.sh <path to prepped data file>` gets top 10 per language.
 - `bin/top10_for_lang.sh <path to prepped data file> <2 letter language code>` gets top 10 for specified language.
 
 *Thanks for your time!*