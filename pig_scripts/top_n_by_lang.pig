-- load file

RAWDAT = LOAD '$input' USING PigStorage() AS (lang:chararray, pagename:chararray, hits:int);

--group by lang
LANG_GROUPED = GROUP RAWDAT BY lang;

TOPN = FOREACH LANG_GROUPED{
  SORTED = ORDER RAWDAT BY hits DESC;
  TRUNCATED = LIMIT SORTED $top_n;
  generate FLATTEN(TRUNCATED);
};

DUMP TOPN;