-- load file

RAWDAT = LOAD '$input' USING PigStorage() AS (lang:chararray, pagename:chararray, hits:int);
LANG_FILTERED = FILTER RAWDAT BY (lang == '$lang');
SORTED = ORDER LANG_FILTERED BY hits DESC;
TOPN = LIMIT SORTED $top_n;
DUMP TOPN;