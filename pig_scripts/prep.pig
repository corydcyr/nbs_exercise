-- load file

RAWDAT = LOAD '$input' USING PigStorage(' ') AS (lang:chararray, pagename:chararray, pagehits:int, servedbytes:int);

--lots of weird things in the language column, for purposes of this
--excersize I'm simply limiting it to the 2 letter language codes.
LANG_FILTERED = FILTER RAWDAT BY SIZE(lang) == 2;

--imperfect method of filtering out 'special' entries, but AFAICT,
--colons as part of the title should be urlencoded
TITLE_FILTERED = FILTER LANG_FILTERED BY INDEXOF(pagename, ':', 0) < 0;

-- strip it down to language, page title, hit count
STOREDAT = FOREACH TITLE_FILTERED GENERATE lang, pagename, pagehits;

--store it using same filename in output dir
STORE STOREDAT INTO '$output';

